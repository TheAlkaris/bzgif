## bzgif

Bzgif is just short for Byzanz GIF. Just a simple script thrown together for [Byzanz](https://github.com/xatgithub/byzanz) a screen recording tool that can create GIFs quick and easy.
Byzanz doesn't come with a GUI, so this is a little something that kind of gives it a GUI to use using [Zenity](https://help.gnome.org/users/zenity/stable/).

## Dependencies
Nothing too much for dependencies but here's what you need


* byzanz
* xrectsel
* LinuxMint Sounds (used for the sounds)
* Suru++ Icons (used in the notifications)

set script executable `chmod +x bzgif.sh`

![screenshot](flameshot_2023-01-04__11_04_13_AM.png "Byzanz GIF Record")

When you run the script you'll be presented with a Zenity window letting you set the number of seconds to screen record for.

There are some pre-set values to quickly use or you can enter your own set time value.

Once you press OK, you'll then be able to draw on your screen the area you want to record your screen by clicking and dragging, and recording will start after a delay of 5 seconds.
