#!/bin/bash

# Byzanz Extended GIF recording
# Semi-GUI tool for Byzanz to make screen record GIFs quick and easy
##
## Created by Alkaris ©2023
##

# Time and Date
# so we can add a timestamp to GIF filename when created.
ts=$(date +"%Y-%m-%d__%I_%M_%S_%p")

# Add a delay before recording in Seconds. We'll set default as 5 seconds
delay=5

# Directory to place recorded GIF
# change this to whatever directory you want.
folder="$HOME/Pictures"

# Default duration of recording in Seconds.
# you can change duration when zenity shows it's window.
duration=10

# Play a little DING sound to let you know when recording is starting/ending
# If you don't have LinuxMint sounds, then you can install it from your distros repository.
beep() {
    paplay /usr/share/sounds/LinuxMint/stereo/system-ready.ogg &
}

# Custom recording duration set by the user
custom_duration=$(zenity --title "Byzanz GIF Record" --text "Record duration?\t\t (default 10)\n\n⚠️Creating GIFs more than 30s will result in large filesize.\t\n"  --entry "" 10 15 20 25 30 2>&1)

# duration and output file
if [ $custom_duration -gt 0 ]; then
     d=$custom_duration
else
     d=$duration
fi

# ------- Xwininfo ---------------------------
# Was trying with Xwininfo, until finding xrectsel is better and simpler.
# Grab window geometry with xwininfo
#XWININFO=$(xwininfo)
#read X < <(awk -F: '/Absolute upper-left X/{print $2}' <<< "$XWININFO")
#read Y < <(awk -F: '/Absolute upper-left Y/{print $2}' <<< "$XWININFO")
#read W < <(awk -F: '/Width/{print $2}' <<< "$XWININFO")
#read H < <(awk -F: '/Height/{print $2}' <<< "$XWININFO")
# ------- Xwininfo ---------------------------

# xsetrel from https://github.com/ropery/xrectsel
# we grab the X & Y coordinates as well as Width and Height when drawn on screen.
geometry=$(xrectsel "--x=%x --y=%y --width=%w --height=%h") || exit -1

# Send notify to notification tray
# If you don't have Suru++ Icons you can install them from your distro's repository
notify-send -i '/usr/share/icons/Suru++/panel/24/kazam-countdown.svg' "Byzanz GIF Recording" "Recording duration set to $d seconds. Recording will start in $delay seconds."

# Now to ACKCHULL recording
sleep $delay
byzanz-record -c --verbose --delay=0 --duration=$d $geometry "$folder/byzanzGIF_$ts.gif"

# Send another one of them notifications to the tray again
notify-send -i '/usr/share/icons/Suru++/panel/24/kazam-countdown.svg' "Byzanz GIF Recording" "Recording has been save to $folder/byzanzGIF_$ts.gif"
